#! /bin/bash
# filename: playbook.sh
# author Ruslan Niyazimbetov <https://nruslan.com>

umask 0002

# Loop through the flags
while [ ! $# -eq 0 ]
do
  case "$1" in

  	# Project/website name
  	--name) if [[ "$2" == *-* ]]; then
        NAME="${2//-/_}"
      else
        NAME="$2"
    fi;;

    # NGINX port
    --nport) NGINX_PORT="$2";;

	# NGINX SSL Port
    --nsport) NGINX_SSL_PORT="$2";;

	# PHP port
    --pport) PHP_PORT="$2";;

	# PHP memory alocation
    --pmemory) PHP_MEMORY="$2";;
	
	# DATABASE port
    --dbport) DB_PORT="$2";;

    --env) ENV="$2";; #local, dev, test, prod

    --exe) EXECUTE=yes;;

	# Installation Path
    --ipath) INSTALLATION_PATH="$2/$NAME";;

  esac
	shift
done



# Path to the script file
SCRIPT_DIR=$(pwd)
	echo "path to the script file: $SCRIPT_DIR"



# Load the Environment Variables
source ".env"
	echo "Environment variables are loaded"



# Check if installation path is set otherwise define a default installation path -------
if [ -z "$INSTALLATION_PATH" ]; then
  INSTALLATION_PATH=$HOME/$NAME
fi
	echo "The files and folders are going to be generated in $INSTALLATION_PATH"



# Set Default Port Values ----------------------------

if [ -z "$NGINX_PORT" ]; then
  NGINX_PORT=$NGINX_DEFAULT_PORT
fi
	echo "NGINX port is $NGINX_PORT"

if [ -z "$NGINX_SSL_PORT" ]; then
  NGINX_SSL_PORT=$NGINX_DEFAULT_SSL_PORT
fi
	echo "NGINX SSL port is $NGINX_SSL_PORT"

# Do not define a PHP port if you are planning to scale
# PHP service
#
#if [ -z "$PHP_PORT" ]; then
#  PHP_PORT=$PHP_DEFAULT_PORT
#fi

if [ -z "$DB_PORT" ]; then
  DB_PORT=$DB_DEFAULT_PORT
fi
	echo "DATABASE port is $DB_PORT"



# Create Necessary Folders -----------------------

mkdir -p "$INSTALLATION_PATH"
cd "$INSTALLATION_PATH" || exit
mkdir "$NGINX_FOLDER" "$PHP_FOLDER" "$DB_FOLDER" "$APP_FOLDER"
	echo "All necessary folders have been created"




# Functions ------------------------------------

function checker() {
  # Check if Docker and Docker-Compose are installed
  which "$1" | grep -o "$1" > /dev/null &&  return 1 || return 0
}

function networks() {
  # Networks
  echo -e "    networks:"
  echo -e "      - ${NAME}net"
}

# Ports
function ports() {
  echo -e "    ports:"
  echo -e "      - \"${1}:${2}\""
}

function build() {
  echo -e "    build:"
  echo -e "      context: ."
  echo -e "      dockerfile: ${2}"
  echo -e "      target: ${1}"
}

function addgroupuser() {
  echo "RUN addgroup -g 1000 ${NAME} && adduser -G ${NAME} -g ${NAME} -s /bin/sh -D ${NAME}"
  echo "RUN mkdir -p /var/www/html"
  echo "RUN chown ${NAME}:${NAME} /var/www/html"
  echo "VOLUME /var/www/html"
}

function set_db_env_vars() {
  sed -i "s/^DB_HOST=.*/DB_HOST=${DB_SERVICE_NAME}/" "${1}"/"${2}"
  sed -i "s/^DB_PORT=.*/DB_PORT=${DB_DOCKER_PORT}/" "${1}"/"${2}"
  sed -i "s/^DB_DATABASE=.*/DB_DATABASE=${MYSQL_DATABASE}/" "${1}"/"${2}"
  sed -i "s/^DB_USERNAME=.*/DB_USERNAME=${MYSQL_USER}/" "${1}"/"${2}"
  sed -i "s/^DB_PASSWORD=.*/DB_PASSWORD=${MYSQL_PASSWORD}/" "${1}"/"${2}"
}



# Set the docker-compose and docker type file --------

touch $DOCKER_COMPOSE_DEV_FILE $DOCKER_COMPOSE_PROD_FILE
touch $DEV_DOCKER_FILE $PROD_DOCKER_FILE

# Create Dockerfile -------

if [ ! -f "$DOCKER_FILE" ]; then
  {
    # NGINX
    echo "FROM ${NGINX_IMAGE} AS ${NGINX_SERVICE_ALIAS}"
    echo "ADD ./${NGINX_FOLDER}/nginx.conf /etc/nginx/"
    echo "ADD ./${NGINX_FOLDER}/default.conf /etc/nginx/conf.d/"
    addgroupuser
    echo ""
    echo ""

    # PHP with App
    echo "FROM ${PHP_IMAGE} AS ${PHP_SERVICE_ALIAS}"
    echo "ADD ./${PHP_FOLDER}/www.conf /usr/local/etc/php-fpm.d/"
    echo "ADD ./${PHP_FOLDER}/${ENV}.ini /usr/local/etc/php/conf.d/${ENV}.ini"
    addgroupuser
    echo "WORKDIR /var/www/html"
    echo "RUN apk add --no-cache tzdata"
    echo "ENV TZ America/Los_Angeles"
    echo "RUN docker-php-ext-install pdo pdo_mysql"
    echo ""
    echo ""

    # Database
    echo "FROM ${MYSQL_IMAGE} AS ${DB_SERVICE_ALIAS}"
    echo ""
    echo ""

    # Composer
    echo "FROM ${COMPOSER_IMAGE} AS ${COMPOSER_SERVICE_ALIAS}"
    echo "RUN addgroup -g 1000 ${NAME} && adduser -G ${NAME} -g ${NAME} -s /bin/sh -D ${NAME}"
    echo "WORKDIR /var/www/html"
  } >> $DOCKER_FILE
fi

echo "Dockerfile has been created"

# Create Docker-Compose file -------
# Docker-Compose file
if [ ! -f "$DOCKER_COMPOSE_FILE" ]; then
  {
    echo "# https://docs.docker.com/compose/compose-file/"
    echo version: \"3.8\"
    echo ""
    echo networks:
    echo -e "  ${NAME}net:"
    echo ""
    echo "# Containers"
    echo services:

    # NGINX Service -------------------------------------------
    echo ""
    echo -e "  # Web Server Container"
    echo -e "  ${NGINX_SERVICE_NAME}:"
    build "$NGINX_SERVICE_ALIAS" $DOCKER_FILE
    # Restart
    echo -e "    restart: unless-stopped"
    # Ports
    ports "${NGINX_PORT}" "${NGINX_DOCKER_PORT}"
    if [ "$ENV" != "$LOCAL" ]; then
      echo -e "      - \"${NGINX_SSL_PORT}:${NGINX_DOCKER_SSL_PORT}\""
    fi
    # Volumes
    echo -e "    volumes:"
    echo -e "      - ./$APP_FOLDER:/var/www/html"
    # Dependencies
    echo -e "    depends_on:"
    echo -e "      - ${PHP_SERVICE_NAME}"
    if [ "$DATABASE" ]
    then
      echo -e "      - ${DB_SERVICE_NAME}"
    fi
    # Networks
    networks

    # PHP and App Service ------------------------------------
    echo ""
    echo -e "  # PHP and App Container"
    echo -e "  ${PHP_SERVICE_NAME}:"
    build "$PHP_SERVICE_ALIAS" $DOCKER_FILE
    # Restart
    echo -e "    restart: unless-stopped"
    if [ "$PHP_MEMORY" ]
    then
      # Deploy
      echo -e "    deploy:"
      echo -e "      resources:"
      echo -e "        limits:"
      echo -e "          memory: ${PHP_MEMORY}M"
      echo -e "        reservations:"
      echo -e "          memory: $((PHP_MEMORY / 2))M"
    fi
    if [ "$PHP_PORT" ]; then
      # Ports
      ports "${PHP_PORT}" "${PHP_DOCKER_PORT}"
    fi
    # Volumes
    echo -e "    volumes:"
    echo -e "      - ./$APP_FOLDER:/var/www/html"
    # TTY
    echo -e "    tty: true"
    # Networks
    networks

    # Database -----------------------------------------------
    echo ""
    echo -e "  # Database Container"
    echo -e "  ${DB_SERVICE_NAME}:"
    build "$DB_SERVICE_ALIAS" $DOCKER_FILE
    echo -e "    user: \"$(id -u):$(id -g)\""
    # Restart
    echo -e "    restart: unless-stopped"
    # TTY
    echo -e "    tty: true"
    # Ports
    ports "${DB_PORT}" "${DB_DOCKER_PORT}"
    # Volumes
    echo -e "    volumes:"
    echo -e "      - ./$DB_FOLDER:/var/lib/mysql"
    echo -e "    environment:"
    echo -e "      MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD}"
    echo -e "      MYSQL_DATABASE: ${MYSQL_DATABASE}"
    echo -e "      MYSQL_USER: ${MYSQL_USER}"
    echo -e "      MYSQL_PASSWORD: ${MYSQL_PASSWORD}"
    echo -e "      SERVICE_TAGS: ${ENV}"
    echo -e "      SERVICE_NAME: ${DB_SERVICE_NAME}"
    # Networks
    networks

    # Composer -----------------------------------------------
    echo ""
    echo -e "  # Composer Container"
    echo -e "  ${COMPOSER_SERVICE_NAME}:"
    build "$COMPOSER_SERVICE_ALIAS" $DOCKER_FILE
    # Volumes
    echo -e "    volumes:"
    echo -e "      - ./$APP_FOLDER:/var/www/html"
    echo -e "    working_dir: /var/www/html"
    # Dependencies
    echo -e "    depends_on:"
    echo -e "      - ${PHP_SERVICE_NAME}"
    echo -e "    entrypoint: [ 'composer', '--ignore-platform-reqs' ]"
    # Networks
    networks

    # Artisan -----------------------------------------------
    echo ""
    echo -e "  # Artisan Container"
    echo -e "  artisan:"
    build "$PHP_SERVICE_ALIAS" $DOCKER_FILE
    # Volumes
    echo -e "    volumes:"
    echo -e "      - ./$APP_FOLDER:/var/www/html"
    echo -e "    working_dir: /var/www/html"
    if [ "$DATABASE" ]; then
      # Dependencies
      echo -e "    depends_on:"
      echo -e "      - ${DB_SERVICE_NAME}"
    fi
    echo -e "    user: ${NAME}"
    echo -e "    entrypoint: [ 'php', '/var/www/html/artisan' ]"
    # Networks
    networks
  } >> $DOCKER_COMPOSE_FILE
fi

echo "Docker-Compose file has been created"

# PHP location
if [ -d "$PHP_FOLDER" ]; then
  # Create .ini and .gitignore files

  {
    echo ${LOCAL}.ini
  } >> .gitignore

  echo ".gitignore file has been created"

  # Add content to .ini file
  {
    echo "upload_max_filesize=40M"
    echo "post_max_size=40M"
  } >> "$PHP_FOLDER/${ENV}.ini"

  cp "$PHP_FOLDER/${ENV}.ini" "$PHP_FOLDER/$DEV.ini"
  cp "$PHP_FOLDER/${ENV}.ini" "$PHP_FOLDER/$PROD.ini"

  echo ".ini files hae been created"

  # Create php-fpm .conf file
  cp "$SCRIPT_DIR"/src/www.conf "$PHP_FOLDER"/www.conf
  sed -i "s/^user.*/user = ${NAME}/" "$PHP_FOLDER"/www.conf
  sed -i "s/^group.*/group = ${NAME}/" "$PHP_FOLDER"/www.conf

  echo "www.conf file for fpm has been created"
fi
# /END PHP location

# NGINX Location -----------------------------------------------------------
if [ -d "$NGINX_FOLDER" ]; then

  # nginx.conf file
  cp "$SCRIPT_DIR"/src/nginx.conf "$NGINX_FOLDER"/nginx.conf
  sed -i "s/^user.*/user ${NAME};/" "$NGINX_FOLDER"/nginx.conf

  # default.conf file
  cp "$SCRIPT_DIR"/src/default.conf "$NGINX_FOLDER"/default.conf

  echo "NGINX .conf files have been created"
fi
# END NGINX Location

# MySQL location -----------------------------------------------------------
if [ -d "$DB_FOLDER" ]; then
  {
    echo "*"
    echo "!.gitignore"
  } >> "$DB_FOLDER"/.gitignore

  echo ".gitignore file in $DB_FOLDER has been created"
fi
# END MySQL Location

# .dockerignore file
if [ ! -f ".dockerignore" ]; then
  {
    echo .git
    echo .idea
  } >> .dockerignore

  echo ".dockerignore file has been created"
fi

# .gitignore file
if [ ! -f ".gitignore" ]; then
  {
    echo /.idea
    echo docker-compose.yml
    echo Dockerfile
  } >> .gitignore

  echo ".gitignore file has been created"
fi

# Execute 
if [ $EXECUTE ]; then
  echo "Executing containers set"

  if [ "$ENV" == "$PROD" ]; then
    echo "Spinning up containers on production server"

  elif [ "$ENV" == "$DEV" ]; then
    echo "Spinning up containers on dev server"

  elif [ "$ENV" == "$LOCAL" ]; then
    echo "Spinning up containers locally"

    # Spin up the containers
    docker-compose up -d --build nginx

    # Execute composer install command
    docker-compose -f ./$DOCKER_COMPOSE_FILE run --rm composer create-project --prefer-dist laravel/laravel .

    # Get PHP container ID
    CONTAINER_NAME=$(docker ps --format "{{.Names}}" | grep "$NAME"_"$PHP_SERVICE_NAME")

    # Go inside the docker container and set the user for storage folder
    docker exec "$CONTAINER_NAME" chown -R "$NAME":"$NAME" storage/
  fi

  sleep 2
  echo ""
  echo "**!** The List of All Running Containers:"
  docker ps

  echo ""
  echo "**!** The folder and file structure are ready. The containers are up and running"
fi